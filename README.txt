It lets you select one of your tax rates as the default for your site and then
sets that to the default when you create a new product.
Basically, it will save you a couple of clicks per product - but that's nice to
have if you've got lots to do.

You can configure from the Module page, or the taxes page:
admin/commerce/config/taxes/commerce_default_tax_rate

If you only have one tax rate in your set up then that rate will be set as the
default when you first install the module.

This is currently a pretty trivial module, but maybe it will save you some time
and help keep one of your users happy!

NOTE:
1/ This does NOT affect items already in the system.
2/ It ONLY affects products added through pages such as node/add/product-display
